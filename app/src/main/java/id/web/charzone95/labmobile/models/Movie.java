package id.web.charzone95.labmobile.models;

/**
 * Created by ASUS on 11/8/2016.
 */

public class Movie {
    private String title, year, genre, poster;

    public Movie() {
    }

    public Movie(String title, String year, String genre, String poster) {
        this.title = title;
        this.year = year;
        this.genre = genre;

        this.poster = poster;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setPoster(String poster) { this.poster = poster; }

    public String getPoster() { return poster; }
}
