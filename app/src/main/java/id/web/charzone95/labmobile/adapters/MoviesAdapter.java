package id.web.charzone95.labmobile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import id.web.charzone95.labmobile.R;
import id.web.charzone95.labmobile.models.Movie;

/**
 * Created by ASUS on 11/8/2016.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {
    private List<Movie> movieList;
    private Context context;

    public MoviesAdapter(List<Movie> movieList) {
        this.movieList = movieList;
    }


    @Override
    public MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie_row, parent, false);

        return new MoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MoviesViewHolder holder, int position) {
        final Movie movie = movieList.get(position);

        holder.tvTitle.setText(movie.getTitle());
        holder.tvYear.setText(movie.getYear());
        holder.tvGenre.setText(movie.getGenre());

        Glide.with(context)
                .load(movie.getPoster())
                .placeholder(android.R.drawable.ic_menu_camera)
                .into(holder.ivPoster);
        Log.d("sesuatulagi", movie.getPoster());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Film: " + movie.getTitle(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MoviesViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvGenre, tvYear;
        public ImageView ivPoster;

        public MoviesViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.title);
            tvGenre = (TextView) itemView.findViewById(R.id.genre);
            tvYear = (TextView) itemView.findViewById(R.id.year);
            ivPoster = (ImageView) itemView.findViewById(R.id.ivPoster);
        }
    }
}
