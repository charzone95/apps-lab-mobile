package id.web.charzone95.labmobile;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import id.web.charzone95.labmobile.adapters.MoviesAdapter;
import id.web.charzone95.labmobile.api.MoviesApi;
import id.web.charzone95.labmobile.models.Movie;
import id.web.charzone95.labmobile.retrofitmodels.MovieRetrofit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter moviesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        moviesAdapter = new MoviesAdapter(movieList);

        //keharusan
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(moviesAdapter);

//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//
//        httpClient.addInterceptor(logging);  // <-- this is the important line!
//
//        Retrofit retrofit = new Retrofit.Builder()
//                                .baseUrl("http://charzone95.web.id/api-lab-mobile/")
//                                .addConverterFactory(GsonConverterFactory.create())
//                                .client(httpClient.build())
//                                .build();

        MoviesApi api = MoviesApi.retrofit.create(MoviesApi.class);

        api.getAllMovies().enqueue(new Callback<ArrayList<MovieRetrofit>>() {
            @Override
            public void onResponse(Call<ArrayList<MovieRetrofit>> call, Response<ArrayList<MovieRetrofit>> response) {
                ArrayList<MovieRetrofit> daftarMovie = response.body();

                for (MovieRetrofit sesuatu : daftarMovie) {
                    Movie movie = new Movie(sesuatu.getTitle(), sesuatu.getYear(), sesuatu.getGenre(), sesuatu.getImage());
                    movieList.add(movie);
                }

                moviesAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ArrayList<MovieRetrofit>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Gagal bro", Toast.LENGTH_SHORT).show();
                Log.e("gagal", t.getMessage());
            }
        });






    }


}
