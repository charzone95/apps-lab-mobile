package id.web.charzone95.labmobile.api;

import java.util.ArrayList;

import id.web.charzone95.labmobile.retrofitmodels.MovieRetrofit;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by ASUS on 11/22/2016.
 */

public interface MoviesApi {

    @GET("movie")
    Call< ArrayList<MovieRetrofit> > getAllMovies();


    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://charzone95.web.id/api-lab-mobile/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
